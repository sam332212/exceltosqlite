#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    excel = new excelToSQLite();
    ui->pushButtonImportTable->setVisible(false);
    ui->pushButtonExportTable->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete excel;
    delete ui;
}

void MainWindow::on_pushButtonDataBaseSelect_clicked()
{
    qDebug("Inside void MainWindow::on_pushButtonDataBaseSelect_clicked()");
    QString dataBasePath = QFileDialog::getOpenFileName(this, "SELECT THE DataBase FILE",QDir::currentPath(),"*.db");
    if(dataBasePath.isEmpty())
    {
        qDebug("DataBase Can not be Empty");
        return;
    }
    ui->pushButtonImportTable->setVisible(true);
    ui->pushButtonExportTable->setVisible(true);
    ui->lineEditDataBasePath->setText(dataBasePath);
    excel->openDb(dataBasePath);
    updateTableList();
    qDebug("Exiting void MainWindow::on_pushButtonDataBaseSelect_clicked()");
}

void MainWindow::on_pushButtonImportTable_clicked()
{
    qDebug("Inside void MainWindow::on_pushButtonImportTable_clicked()");
    QString excelFilePath = QFileDialog::getOpenFileName(this, "Select The Excel FILE",QDir::currentPath(),"*.xlsx");
    if(excelFilePath.isEmpty())
    {
        qDebug("excelFilePath Can not be Empty");
    }
    qDebug()<<"excelFilePath = "<<excelFilePath;
//    ui->lineEditDataBasePath->setText(excelFilePath);

    excel->openExcel(excelFilePath);
    updateTableList();
    qDebug("Exiting void MainWindow::on_pushButtonImportTable_clicked()");
}

void MainWindow::on_pushButtonExportTable_clicked()
{
    qDebug("Inside void MainWindow::on_pushButtonExportTable_clicked()");
    QString tableName = ui->comboBoxTablesList->currentText();
    qDebug()<<"Current Table Selected: "<<tableName;
    QString excelFilePath = QFileDialog::getSaveFileName(this,"ENTER THE FILE NAME",QDir::homePath(),"*.xlsx");
    if(excelFilePath.isEmpty()) {
        qDebug("Please Enter a Valid Name for the file");
        return;
    }
    qDebug()<<"Excel File Name: "<< excelFilePath;
    excel->createExcel(tableName, excelFilePath);
    qDebug("Exiting void MainWindow::on_pushButtonExportTable_clicked()");
}

void MainWindow::on_comboBoxTablesList_currentIndexChanged(const QString &arg1)
{
    qDebug("Inside void MainWindow::on_comboBoxTablesList_currentIndexChanged(const QString &arg1)");
    qDebug()<<"Current Table: "<<arg1;
    QSqlTableModel *tablemodel = excel->getSqlTable(arg1);
    ui->tableView->setModel(tablemodel);
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);
    qDebug("Exiting void MainWindow::on_comboBoxTablesList_currentIndexChanged(const QString &arg1)");
}

void MainWindow::updateTableList()
{
    qDebug("Inside void MainWindow::updateTableList()");
    ui->comboBoxTablesList->clear();
    ui->comboBoxTablesList->addItems(excel->getTablesList());
    qDebug("Exiting void MainWindow::updateTableList()");
}
