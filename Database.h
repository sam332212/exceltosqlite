/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Database.h
*  Creation date and time - 31-AUG-2019 , SAT 03:56 PM IST
*  Author: - Manoj
*  Purpose of the file - All sqlite database handling modules will be declared .
*
**********************************************************************************************************************/
#ifndef DATABASE_H
#define DATABASE_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QDebug>
#include <QtSql>
#include <QSqlError>
#include <QMessageBox>
#include <QSqlRecord>
#include <QCoreApplication>
#include <QSqlTableModel>


//#include "Timer.h"

/********************************************************
 Class Declaration
 ********************************************************/
class Database : public QObject
{
   Q_OBJECT
private:
    //member variables
    QSqlDatabase db;
//    Timer timerObj;
    QSqlTableModel *model = nullptr;

    //member functions
    void generateQueryForCreation(QString tableName,QString& queryGenerated,QStringList tableColumnNames);
    void generateQueryForInsertion(QString tableName,QString& queryGenerated,QStringList& bindValues);
    void generateQueryForDeletion(QString tableName,QString& queryGenerated,QStringList& bindValues,QStringList data);
    void generateQueryForSelection(QString tableName,QString& queryGenerated,QStringList& bindValues,QString matchData,QStringList data);
    void generateQueryForUpdation(QString tableName,QString& queryGenerated,QStringList& bindValues,QStringList dataToUpdate,QStringList dataToMatch);
    void generateQueryForclearTable(QString tableName,QString& queryGenerated);

public:
    //member functions
    Database();
    ~Database();

    void closeDB();
    void removeDB(QString connectionName);
    void setFilter(QSqlTableModel *model, QString filter);

    bool openDB(QString dbName,QString hostName,QString UserName, QString Password,QString connectionName);
    bool openDB(QString dbName);
    bool isOpen();
    bool createTable(QString tableName,QStringList columnNames);
    bool addBlankRows(QString TableName, int NumberOfRows);
    bool addMultipleRowsData(QString tableName, QVector<QStringList> rowValues);
    bool addMultipleRowsData(QString tableName, QStringList columnNames, QVector<QStringList> rowValues);
    bool addSingleRowData(QString tableName,QStringList rowValue);
    bool editCell(QString tableName, unsigned rowNumber,QString columnName, QString newCellValue);
    bool editCell(QString tableName, QString columnName,QString cellValue, QString newCellValue);
    bool editRowData(QString tableName, unsigned rowID,QStringList newRowValues);
    bool editRowData(QString tableName, QString columnName,QString cellValue, QStringList newRowValue);
    bool deleteTables(QStringList tablesToDelete);
    bool deleteTables();
    bool deleteTable(QString tableName);
    bool deleteRows(QString tableName, QString columnName,QString cellValue);
    bool deleteRows(QString tableName, QString cellValue);
    bool deleteRow(QString tableName, int rowNumber);
    bool deleteRow(QString tableName);
    bool cleanTable(QString tableName);
    bool getTableData(QString tableName,QVector<QStringList>& tableData);
    bool getLastTableRowData(QString tableName,QStringList& rowData);
    bool getRowContents(QString tableName, int ROWID,QStringList &rowContents);
    bool getRowContents(QString tableName, QString columnName,QString currentCellValue, QStringList& rowContents);
    bool getColumnNames(QString TableName, QStringList &ColumnNames);
    bool getColumnNames(QSqlTableModel* model, QStringList &ColumnNames);
    bool getColumnContents(QString tableName, QString columnName, QStringList &columnContents);
    bool getColumnContents(QSqlDatabase DB, QString tableName, QString columnName, QStringList &columnContents);
    bool getCellContent(QString tableName, QString columnName,unsigned ROWID, QString &cellContent);
    bool performVacuum();
    bool isDataPresentInColumn(QString TableName, QString ColumnName, QString CellValue);
    bool renameTable(QString TableName, QString NewName);
    bool isTableNameValid(QString tableName);

    QString getDBName();
    QString lastError();
    QStringList getTableNames();
    QSqlDatabase getDB();
    QSqlTableModel* getDatabaseModel(QString tableName);
    QSqlTableModel* getDatabaseModel(QString tableName, QSqlDatabase db);
    QVariant getValueFromModel(QSqlTableModel *model, int row, int col);

    unsigned getRowID(QString TableName, QString ColumnName,QString CurrentCellValue);
    unsigned getRowCount(QString TableName);
    unsigned getColumnCount(QString TableName);

};

#endif // DATABASE_H
