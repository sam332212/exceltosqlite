#ifndef EXCELTOSQLITE_H
#define EXCELTOSQLITE_H
#include<QString>
#include<QStringList>
#include"Database.h"
#include "Database/SqlDatabaseClass.h"
#include "Excel/qtxlsx/src/xlsx/xlsxdocument.h"
#include "Excel/qtxlsx/src/xlsx/xlsxcellrange.h"
#include "Excel/qtxlsx/src/xlsx/xlsxcell.h"
#include "Excel/qtxlsx/src/xlsx/xlsxformat.h"

class excelToSQLite
{
public:
    excelToSQLite();
    ~excelToSQLite();
    int createTable(QStringList strList);
    void openDb(QString dataBasePath);
    void openExcel(QString excelFilePath);
    void createExcel(QString tableName,QString ExcelFilepath);
    QStringList getTablesList();
    QSqlTableModel* getSqlTable(QString TableName);

private:
    Database *database = nullptr;
};

#endif // EXCELTOSQLITE_H
