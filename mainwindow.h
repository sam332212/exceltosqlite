#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QDebug>
#include "exceltosqlite.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void updateTableList();

private slots:
    void on_pushButtonDataBaseSelect_clicked();

    void on_pushButtonImportTable_clicked();

    void on_pushButtonExportTable_clicked();

    void on_comboBoxTablesList_currentIndexChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    excelToSQLite *excel = nullptr;
};

#endif // MAINWINDOW_H
