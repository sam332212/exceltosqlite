/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Database.cpp
*  Creation date and time - 31-AUG-2019 , SAT 03:56 PM IST
*  Author: - Manoj
*  Purpose of the file - All sqlite database handling modules will be implemented .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Database.h"
#include "debugflag.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if DATABASE_DEBUG
    #define _qDebug(s) qDebug()<<"\n"<<s
#else
    #define _qDebug(s);
#endif

#if DATABASE_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s);
#endif

/********************************************************
 Function Definition
 ********************************************************/

/*************************************************************************
 Function Name - Database
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
Database::Database()
{
    _qDebug("inside Database Constructor.");
}

/*************************************************************************
 Function Name - ~Database
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
Database::~Database()
{
    _qDebug("inside Database Destructor.");
}

/*************************************************************************
 Function Name - openDB
 Parameter(s)  - QString,QString,QString,QString,QString
 Return Type   - bool
 Action        - Opens the db. Returns false if open fails or else returns
                 true.
 *************************************************************************/
bool Database::openDB(QString dbName,QString hostName,QString userName, QString password,QString connectionName)
{
    _qDebug("inside bool Database::openDB(QString dbName).");

    bool ret = true;

    db = QSqlDatabase :: addDatabase("QSQLITE",connectionName);
    db.setDatabaseName(dbName);
    db.setHostName(hostName);
    db.setUserName(userName);
    db.setPassword(password);

    if (!db.isValid()){
        QMessageBox::critical(nullptr,QObject::tr("Invalid Database"),
                              db.lastError().text());
        ret = false;
    }
    else if (!db.open(userName,password)) {
        QMessageBox::critical(nullptr,QObject::tr("Database Error"),
                              db.lastError().text());
        ret = false;
    }

    return ret;
}

/*************************************************************************
 Function Name - openDB
 Parameter(s)  - QString
 Return Type   - bool
 Action        - Opens the db. Returns false if open fails or else returns
                 true.
 *************************************************************************/
bool Database::openDB(QString dbName)
{
    _qDebug("inside bool Database::openDB(QString dbName).");

    bool ret = true;

    db = QSqlDatabase :: addDatabase("QSQLITE");
    db.setDatabaseName(dbName);
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("123");

    if (!db.isValid()){
        QMessageBox::critical(nullptr,QObject::tr("Invalid Database"),
                              db.lastError().text());
        ret = false;
    }
    else if (!db.open("root","123")) {
        QMessageBox::critical(nullptr,QObject::tr("Database Error"),
                              db.lastError().text());
        ret = false;
    }

    return ret;
}

/*************************************************************************
 Function Name - closeDB
 Parameter(s)  - void
 Return Type   - void
 Action        - Closes the db if open.
 *************************************************************************/
void Database::closeDB()
{
    _qDebug("inside void Database::closeDB().");

    if(db.isOpen()){
        _qDebugE("Db is open. Closing Now.","","","");

        db.close();
        db = QSqlDatabase();
        QSqlDatabase::removeDatabase(db.connectionName());
    }
    else {
        _qDebugE("Db is not open.","","","");
    }
}

/*************************************************************************
 Function Name - removeDB
 Parameter(s)  - QString
 Return Type   - void
 Action        - removes DB connection from connection name. This function
                 must be called after closing DB(QSqlDatabase::close()).
 *************************************************************************/
void Database::removeDB(QString connectionName)
{
    _qDebug("inside void Database::removeDB(QString connectionName).");

    db.removeDatabase("QSQLITE");
    db.removeDatabase(connectionName);
}

/*************************************************************************
 Function Name - isOpen
 Parameter(s)  - void
 Return Type   - bool
 Action        - returns true if db is open else returns false.
 *************************************************************************/
bool Database::isOpen()
{
    _qDebug("inside bool Database::isOpen().");

    if(db.isOpen())
        return true;
    else
        return false;
}

/*************************************************************************
 Function Name - createTable
 Parameter(s)  - QString , QStringList
 Return Type   - bool
 Action        - creates a new table named tableName with given column names
                 in tableColumnNames. Returns false if table creation fails
                 or else returns true.
 *************************************************************************/
bool Database::createTable(QString tableName,QStringList tableColumnNames)
{
    _qDebug("inside bool Database::createTable(QString tableName,QStringList tableColumnNames).");

    _qDebugE("table Name : ",tableName,"","");
    _qDebugE("tableColumnNames : ",tableColumnNames,"","");

    bool ret = true;
    QString queryGenerated;
    QSqlQuery query(db);
    query.exec("VACUUM");

    if((tableName.size() == 0) || (tableColumnNames.size() == 0))
    {
        _qDebugE("Oops!! Table name and column names can't be Empty.","","","");

        ret = false;
    }
    else if (db.isOpen())
    {
        generateQueryForCreation(tableName,queryGenerated,tableColumnNames);

        ret = query.exec(queryGenerated);
        //Ex- ret = query.exec("create table trTable(\"To\" varchar(500), \"From\" varchar(500))");
        if(ret)
        {
            qDebug()<<"Table created Successfully";
        }
        query.finish();
        query.clear();
    }
    else
    {
        _qDebugE("Oops!! Database is not open.","","","");

        ret = false;
    }
    return ret;
}

/*************************************************************************
 Function Name - addBlankRows
 Parameter(s)  - QString, int
 Return Type   - bool
 Action        - adds/appends numberOfRows quantity of blank rows to given
                 tableName.
 *************************************************************************/
bool Database::addBlankRows(QString tableName, int numberOfRows)
{
    _qDebug("Inside bool Database::addBlankRows(QString tableName, int numberOfRows).");
    bool ret = true;

    QSqlTableModel *model = nullptr;
    QStringList columnNames;
    QSqlRecord record;

    if((tableName.size() == 0) || (numberOfRows == 0))
    {
        _qDebugE("Oops!! TableName or NumberOfRows can't be Empty.","","","");

        ret = false;
    }
    else
    {
        model = new QSqlTableModel(this,db);

        if(model != nullptr)
        {
            model->setTable(tableName);
            model->setEditStrategy(QSqlTableModel::OnManualSubmit);

            //Get column names first
            columnNames.clear();
            getColumnNames(model,columnNames);

            //Now add blank rows
            for(int rowNo=0; rowNo<numberOfRows; rowNo++)
            {
                record = model->record();
                for(int colNo=0; colNo<columnNames.count(); colNo++)
                {
                    record.setValue(columnNames.at(colNo), "");
                }
                model->insertRecord(-1, record); //adds the row at the end
            }
            model->submitAll();
            clearmem(model);
        }
        else {
            ret = false;
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - addMultipleRowsData
 Parameter(s)  - QString , QStringList, QVector<QStringList>
 Return Type   - bool
 Action        - adds/appends the rows with rowData to the table given in
                 tableName and the number of rows are equal to the number
                 of QStringList data present in the given QStringList vector(rowValues).
                 Here data is filled under the shell of the given column name
                 and other shell of the row remains blank(NULL)
 *************************************************************************/
bool Database::addMultipleRowsData(QString tableName, QStringList columnNames, QVector<QStringList> rowValues)
{
    _qDebug("Inside bool Database::addMultipleRowsData(QString tableName, QStringList columnNames, QVector<QStringList> rowValues).");

    bool ret = true;
    int rowCount;

    QString values;
    QString tempData;
    QStringList rowData;
    QString columName;
    QString query;
    QSqlQuery qry(db);

    performVacuum();

    if((tableName.size() == 0) || (columnNames.size() == 0) || (rowValues.size() == 0))
    {
        _qDebugE("Oops!! TableName or ColumnNames or RowValues can't be Empty.","","","");

        ret = false;
    }
    else
    {
        rowCount = rowValues.count();

        values.clear();

        //Prepare Column Name String
        for(int i=0; i<columnNames.count(); i++)
        {
            columName += "\"" + columnNames.at(i) + "\"";
            if(i!=columnNames.count()-1)
            {
                columName +=", ";
            }
        }

        for(int rowNo=0; rowNo < rowCount; rowNo++)
        {
            values += "(";

            rowData = rowValues.at(rowNo);

            for(int j=0; j<rowData.count(); j++)
            {
                values += "\""+ rowData.at(j) +"\"";
                if(j!=rowData.count()-1)
                {
                    values += ", ";
                }
            }
            values +=")";
            if(rowNo!=rowValues.count()-1)
            {
                values += ", ";
            }
        }//for loop runs for number of table rows

        query = QCoreApplication::tr("insert into %1 (%2) values %3").arg(tableName).arg(columName).arg(values);

        _qDebugE("columName:[",columName,"]","");
        _qDebugE("values:[",values,"]","");
        _qDebugE("query:[",query,"]","");

        /* Ex-
         * ColumName:[ "\"MeterName\", \"PortName\"" ]
         * values:[ "(\"5\", \"6\"), (\"7\", \"8\")" ]
         * query:[ "insert into MeterSettings (\"MeterName\", \"PortName\") values (\"5\", \"6\"), (\"7\", \"8\")" ]
         *
         * or(Only for specific column(s) data entry)====>
         * ColumName:[ "\"MeterName\"" ]
         * values:[ "(\"5\"), (\"7\")" ]
         * query:[ "insert into MeterSettings (\"MeterName\") values (\"5\"), (\"7\")" ]
         */
        ret = qry.exec(query);

        if(!ret){
            _qDebugE(qry.lastError().text(),"","","");
        }
        else{
            _qDebugE("QUERY IS:[",qry.lastQuery(),"]","");
        }

        qry.finish();
        qry.clear();
    }//else block executes when input data is valid(non-empty)

    return ret;
}

/*************************************************************************
 Function Name - addMultipleRowsData
 Parameter(s)  - QString , QVector<QStringList>
 Return Type   - bool
 Action        - adds/appends the rows with rowData to the table given in
                 tableName and the number of rows are equal to the number
                 of QStringList data present in the given QStringList vector(rowValues).
 *************************************************************************/
bool Database::addMultipleRowsData(QString tableName, QVector<QStringList> rowValues)
{
    _qDebug("Inside bool Database::addMultipleRowsData(QString tableName, QVector<QStringList> rowValues).");

    bool ret = true;
    int rowCount;

    QString values;
    QString tempData;
    QStringList rowData;
    QString query;
    QSqlQuery qry(db);

    performVacuum();

    if((tableName.size() == 0) || (rowValues.size() == 0))
    {
        _qDebugE("Oops!! TableName or RowValues can't be Empty.","","","");

        ret = false;
    }
    else
    {
        rowCount = rowValues.count();

        values.clear();

        for(int rowNo=0; rowNo < rowCount; rowNo++)
        {
            if(rowNo % 10 == 0)
            {
                //giving delay for smoothing the process
//                timerObj.delayInms(1);
            }

            values += "(";
            rowData = rowValues.at(rowNo);
            for(int j=0; j<rowData.count(); j++)
            {
                tempData = rowData.at(j);

                //The below code is for handling double quotes
                if(tempData.contains("\""))
                {
                    tempData.replace("\"", "\"\"");
                }

                values += "\""+ tempData +"\"";

                if(j!=rowData.count()-1)
                {
                    values += ", ";
                }
            }
            values +=")";

            //Below "," indicates next row starts
            if(rowNo!=rowValues.count()-1)
            {
                values += ", ";
            }
        }//for loop runs for number of table rows

        query = QCoreApplication::tr("insert into %1 values %2").arg(tableName).arg(values);

        _qDebugE("rowValues:[",rowValues,"]","");
        _qDebugE("values:[",values,"]","");
        _qDebugE("query:[",query,"]","");

        /* Ex-
         * RowValues:[ QVector(("1", "2"), ("3", "4")) ]
         * Values:[ "(\"1\", \"2\"), (\"3\", \"4\")" ]
         * query:[ "insert into MY_TABLE values (\"1\", \"2\"), (\"3\", \"4\")" ]
         */

        ret = qry.exec(query);

        if(!ret){
            _qDebugE(qry.lastError().text(),"","","");
        }
        else{
            _qDebugE("QUERY IS:[",qry.lastQuery(),"]","");
        }

        qry.finish();
        qry.clear();
    }//else block executes when input data is valid(non-empty)

    return ret;
}

/*************************************************************************
 Function Name - addSingleRowsData
 Parameter(s)  - QString , QStringList
 Return Type   - bool
 Action        - adds/appends a row with rowValue to the table given in
                 tableName.
 *************************************************************************/
bool Database::addSingleRowData(QString tableName,QStringList rowValue)
{
    _qDebug("Inside bool Database::addSingleRowData(QString tableName,QStringList rowValue).");

    bool ret = true;

    QString values;
    QString tempData;
    QString query;
    QSqlQuery qry(db);

    performVacuum();

    if((tableName.size() == 0) || (rowValue.size() == 0))
    {
        _qDebugE("Oops!! TableName or RowValue can't be Empty.","","","");

        ret = false;
    }
    else
    {
        values += "(";

        for(int j=0; j<rowValue.count(); j++)
        {
            tempData = rowValue.at(j);

            //The below code is for handling double quotes
            if(tempData.contains("\""))
            {
                tempData.replace("\"", "\"\"");
            }

            values += "\""+ tempData +"\"";

            if(j!=rowValue.count()-1)
            {
                values += ", ";
            }
        }
        values +=")";

        query = QCoreApplication::tr("insert into %1 values %2").arg(tableName).arg(values);

        _qDebugE("given rowValue:[",rowValue,"]","");
        _qDebugE("values:[",values,"]","");
        _qDebugE("query:[",query,"]","");

        /* Ex-
         * given rowValue:[ ("1", "2") ]
         * values:[ "(\"1\", \"2\")" ]
         * query:[ "insert into MeterSettings values (\"1\", \"2\")" ]
         */

        ret = qry.exec(query);

        if(!ret){
            _qDebugE(qry.lastError().text(),"","","");
        }
        else{
            _qDebugE("QUERY IS:[",qry.lastQuery(),"]","");
        }

        qry.finish();
        qry.clear();
    }//else block executes when input data is valid(non-empty)

    return ret;
}

/*************************************************************************
 Function Name - editCell
 Parameter(s)  - QString, unsigned, QString, QString
 Return Type   - bool
 Action        - changes the value of the cell present at [rowNumber,columnName]
                 with the newCellValue.
 *************************************************************************/
bool Database::editCell(QString tableName, QString columnName,
                        QString cellValue, QString newCellValue)
{
    _qDebug("Inside bool Database::editCell(QString tableName, QString columnName,\
            QString cellValue, QString newCellValue).");

    bool ret = true;
    unsigned rowCount;
    unsigned rowID;

    QString query;
    QSqlQuery qry(db);

    performVacuum();

    if((tableName.size() == 0) || (columnName.size() == 0) || (cellValue.size() == 0))
    {
        _qDebugE("Oops!! TableName or ColumnName or CellValue can't be Empty.","","","");

        ret = false;
    }
    else
    {
        rowID = getRowID(tableName,columnName,cellValue);
        rowCount = getRowCount(tableName);

        _qDebugE("rowID:",rowID,"","");
        _qDebugE("rowCount:",rowCount,"","");

        if((rowCount < rowID) || (rowID == 0))
        {
            _qDebugE("Oops!! invalid rowID fetched","","","");
            ret = false;
        }
        else
        {
            query = QCoreApplication::tr("UPDATE %1 SET %2= \"%3\" WHERE ROWID=%4").arg(tableName).arg(columnName).arg(newCellValue).arg(QString::number(rowID));

            ret = qry.exec(query);

            if(!ret)
            {
               _qDebugE("Query execution failed for editing cell.","","","");
            }
            else
            {
                qry.finish();
                qry.clear();
            }
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - editCell
 Parameter(s)  - QString, unsigned, QString, QString
 Return Type   - bool
 Action        - changes the value of the cell present at [rowNumber,columnName]
                 with the newCellValue.
 *************************************************************************/
bool Database::editCell(QString tableName, unsigned rowNumber,
                        QString columnName, QString newCellValue)
{
    _qDebug("Inside bool Database::editCell(QString tableName, unsigned rowNumber,\
            QString columnName, QString newCellValue).");

    bool ret = true;
    unsigned int rowCount;

    QString query;
    QSqlQuery qry(db);

    if((tableName.size() == 0) || (columnName.size() == 0) || (rowNumber == 0))
    {
        _qDebugE("Oops!! TableName or ColumnName or RowNumber can't be Empty.","","","");

        ret = false;
    }
    else
    {
        rowCount = getRowCount(tableName);

        if(rowCount < rowNumber)
        {
            _qDebugE("Oops!! Invalid Row Number.","","","");
            ret = false;
        }
        else
        {
            query = QCoreApplication::tr("UPDATE %1 SET %2=\"%3\" WHERE ROWID=%4").arg(tableName).arg(columnName)
                .arg(newCellValue).arg(QString::number(rowNumber));

            ret = qry.exec(query);

            if(!ret)
            {
               _qDebugE("Query execution failed for Editing Cell.","","","");
            }
            else
            {
                qry.finish();
                qry.clear();
            }
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - editRowData
 Parameter(s)  - QString, unsigned, QStringList
 Return Type   - bool
 Action        - updates the data in row of rowID with newRowValue.
 *************************************************************************/
bool Database::editRowData(QString tableName, unsigned rowID,QStringList newRowValue)
{
    _qDebug("Inside bool Database::editRowData(QString tableName, unsigned rowID,QStringList newRowValues).");

    bool ret = true;

    QString query;
    QString fields;
    QString values;
    QString tempStr;
    QStringList columnList;

    QSqlQuery qry(db);

    performVacuum();

    if((tableName.size() == 0) || (newRowValue.size() == 0) || (rowID == 0))
    {
        _qDebugE("Oops!! TableName or RowID or NewRowValue can't be Empty.","","","");

        ret = false;
    }
    else
    {
        columnList.clear();
        ret = getColumnNames(tableName,columnList);

        if(!ret)
        {
            _qDebugE("Getting Column Lists Failed.","","","");
        }
        else
        {
            //Prepare fields
            fields.clear();
            for(int i=0; i<columnList.count(); i++)
            {
                fields += columnList.at(i);
                if(i < columnList.count()-1)
                {
                    fields += ", ";
                }
            }

            //prepare values
            for(int i=0; i<newRowValue.count(); i++)
            {
                tempStr = "\"";
                tempStr += newRowValue.at(i);
                tempStr += "\"";

                values += tempStr;
                if(i < newRowValue.count()-1)
                {
                    values += ", ";
                }
            }

            //Prepare statement
            query = QCoreApplication::tr("UPDATE %1 SET (%2) = (%3) WHERE ROWID=%4").arg(tableName)
                            .arg(fields).arg(values).arg(rowID);

            _qDebugE("fields:",fields,"","");
            _qDebugE("values:",values,"","");
            _qDebugE("query:[",query,"]","");

            /* Ex-
             * fields: "MeterName, PortName"
             * values: "\"90\", \"91\""
             * query:[ "UPDATE MeterSettings SET (MeterName, PortName) = (\"90\", \"91\") WHERE ROWID=3" ]
             */
            ret = qry.exec(query);

            if(!ret)
            {
               _qDebugE("Query execution failed at updating Row.","","","");
            }
            else
            {
                qry.finish();
                qry.clear();
            }
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - editRowData
 Parameter(s)  - QString, QString, QString, QStringList
 Return Type   - bool
 Action        - searches the row having given columnName and cellValue and
                 updates the data in that row with newRowValue.
 *************************************************************************/
bool Database::editRowData(QString tableName, QString columnName,QString cellValue, QStringList newRowValue)
{
    _qDebug("Inside bool Database::editRowData(QString tableName, QString columnName,QString cellValue, QStringList newRowValue).");

    bool ret = true;

    unsigned rowId = getRowID(tableName, columnName, cellValue);

    if(rowId == 0)
    {
        ret = false;
    }
    else
    {
        ret = editRowData(tableName,rowId, newRowValue);
    }

    return ret;
}

/*************************************************************************
 Function Name - deleteTables
 Parameter(s)  - QStringList
 Return Type   - bool
 Action        - deletes multiple tables given in parameterList "tablesToDelete".
 *************************************************************************/
bool Database::deleteTables(QStringList tablesToDelete)
{
    _qDebug("Inside bool Database::deleteTables(QStringList tablesToDelete).");

    bool ret = true;

    QStringList allTables = getTableNames();

    for(int i=0; i<tablesToDelete.count(); i++)
    {
        if(allTables.contains(tablesToDelete.at(i),Qt::CaseInsensitive))
        {
            if(!deleteTable(tablesToDelete.at(i)))
            {
                ret = false;
            }
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - deleteTables
 Parameter(s)  - void
 Return Type   - bool
 Action        - deletes all tables present in database.
 *************************************************************************/
bool Database::deleteTables()
{
    _qDebug("Inside bool Database::deleteTables().");

    bool ret = true;

    QStringList allTables = getTableNames();

    for(int i=0; i<allTables.count(); i++)
    {
        if(!deleteTable(allTables.at(i)))
        {
            ret = false;
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - deleteTable
 Parameter(s)  - QString
 Return Type   - bool
 Action        - deletes the given table in tableName.
 *************************************************************************/
bool Database::deleteTable(QString tableName)
{
    _qDebug("inside bool Database::deleteTable(QString tableName)).");

    bool ret = true;

    QSqlQuery qry(db);
    QString query;

    performVacuum();

    if(tableName.size() == 0)
    {
        _qDebugE("Oops!! Table name can't be Empty.","","","");

        ret = false;
    }
    else
    {
        query = "DROP TABLE IF EXISTS " + tableName;

        ret = qry.exec(query);

        if(!ret){
            _qDebugE(qry.lastError().text(),"","","");
        }
        else{
            _qDebugE("QUERY IS:[",qry.lastQuery(),"]","");
        }

        qry.finish();
        qry.clear();

        //OR=============
/*
        ret = qry.prepare("DROP TABLE IF EXISTS " + tableName +";");
        if(!ret)
        {
            _qDebugE("Oops!! query preparation failed.","","","");
        }
        else
        {
            ret = qry.exec();

            if(!ret){
                _qDebugE(qry.lastError().text(),"","","");
            }
            else{
                _qDebugE("QUERY IS:[",qry.lastQuery(),"]","");
            }

            qry.finish();
            qry.clear();
        }//else block executes when query prepared successfully
*/
    }

    return  ret;
}

/*************************************************************************
 Function Name - deleteRows
 Parameter(s)  - QString, QString, QString
 Return Type   - bool
 Action        - deletes multiple rows which contains cellValue at columnName.
 *************************************************************************/
bool Database::deleteRows(QString tableName,QString columnName,QString cellValue)
{
    _qDebug("Inside bool Database::deleteRows(QString tableName,QString columnName,QString cellValue).");

    bool ret = true;

    QString query;
    QSqlQuery qry(db);

    performVacuum();

    if((tableName.size() == 0) || (columnName.size() == 0) || (cellValue.size() == 0))
    {
        _qDebugE("Oops!! TableName or ColumnName or CellValue can't be Empty.","","","");

        ret = false;
    }
    else
    {
        query = QCoreApplication::tr("DELETE FROM %1 WHERE \"%2\"=\"%3\"").arg(tableName).arg(columnName).arg(cellValue);

        ret = qry.exec(query);

        if(!ret)
        {
           _qDebugE("Query execution failed for Deleting Row.","","","");
        }
        else
        {
            qry.finish();
            qry.clear();
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - deleteRows
 Parameter(s)  - QString, QString
 Return Type   - bool
 Action        - deletes multiple rows which contains cellValue at any column.
 *************************************************************************/
bool Database::deleteRows(QString tableName, QString cellValue)
{
    _qDebug("Inside bool Database::deleteRows(QString tableName, QString cellValue).");

    bool ret = true;
    unsigned int rowCount = 0;

    QString query;
    QString rowsToDelete;
    QString qryStr;
    QStringList rowNumberList;
    QSqlQuery qry(db);

    performVacuum();

    if((tableName.size() == 0) || (cellValue.size() == 0))
    {
        _qDebugE("Oops!! TableName or CellValue can't be Empty.","","","");

        ret = false;
    }
    else
    {
        rowCount = getRowCount(tableName);

        rowsToDelete.clear();
        rowNumberList.clear();

        for(unsigned int rowNo=1; rowNo <= rowCount; rowNo++)
        {
            query = QCoreApplication::tr("select * from %1  where rowid=%3").arg(tableName).arg(QString::number(rowNo));

            ret = qry.exec(query);

            //ret = qry.exec("select * from "+tableName+" where rowid="+QString::number(i));

            if(!ret)
            {
                _qDebugE("Query execution failed for Selecting Row.","","","");
                break;
            }
            else
            {
                while(qry.next())
                {
                    for(int j=0; j<qry.record().count(); j++)
                    {
                        qryStr = qry.value(j).toString();
                        qryStr.replace(" ","");

                        if(qryStr.compare(cellValue) == 0)
                            rowsToDelete += QString::number(rowNo) + ",";
                    }
                }

                qry.finish();
                qry.clear();
            }
        }

        if(rowsToDelete.count() == 0)
        {
            ret = false;
        }

        if(ret)
        {
            rowsToDelete.replace(" ","");

            rowNumberList = rowsToDelete.split(",");

            for(int i=0; i<rowNumberList.count(); i++)
            {
                query = "delete from "+tableName+" where rowid="+rowNumberList.at(i);


                ret = qry.exec(query);

                if(!ret)
                {
                    _qDebugE("Oops!! Query execution failed for Deleting Row.","","","");

                    ret = false;
                    break;
                }
            }
            qry.finish();
            qry.clear();
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - deleteRow
 Parameter(s)  - QString, int
 Return Type   - bool
 Action        - deletes the row whose number matched with given rowNumber.
 *************************************************************************/
bool Database::deleteRow(QString tableName, int rowNumber)
{
    _qDebug("Inside bool Database::deleteRow(QString tableName, int rowNumber).");

    bool ret = true;
    int rowCount;

    QString query;
    QSqlQuery qry(db);

    performVacuum();

    if((tableName.size() == 0) || (rowNumber == 0))
    {
        _qDebugE("Oops!! TableName or RowNumber can't be Empty.","","","");

        ret = false;
    }
    else
    {
        query = QCoreApplication::tr("SELECT COUNT(*) FROM %1").arg(tableName);

        ret = qry.exec(query);

        if(!ret)
        {
            _qDebugE("Query execution failed for getting Rowcount.","","","");
        }
        else
        {
            if(qry.next())
            {
                rowCount = qry.value(0).toInt();
                if(rowCount < rowNumber)
                {
                    _qDebugE("Oops! Invalid RowNumber.","","","");

                    ret = false;
                }
            }

            qry.finish();
            qry.clear();
        }
    }

    if(ret)
    {
        query = "DELETE FROM "+tableName+" WHERE ROWID="+QString::number(rowNumber);

        ret = qry.exec(query);

        if(!ret)
        {
            _qDebugE("Query execution failed for deleting RowNumber.","","","");
        }
        else
        {
            qry.finish();
            qry.clear();
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - deleteRow
 Parameter(s)  - QString
 Return Type   - bool
 Action        - deletes the Last row.
 *************************************************************************/
bool Database::deleteRow(QString tableName)
{
    _qDebug("Inside bool Database::deleteRow(QString tableName).");

    bool ret = true;
    int rowCount = 0;

    QString query;
    QSqlQuery qry(db);

    performVacuum();

    if(tableName.size() == 0)
    {
        _qDebugE("Oops!! TableName can't be Empty.","","","");

        ret = false;
    }
    else
    {
        query = QCoreApplication::tr("SELECT COUNT(*) FROM %1").arg(tableName);

        ret = qry.exec(query);

        if(!ret)
        {
            _qDebugE("Query execution failed for getting Rowcount.","","","");
        }
        else
        {
            if(qry.next())
            {
                rowCount = qry.value(0).toInt();
            }

            qry.finish();
            qry.clear();
        }
    }

    if(ret)
    {
        query = "DELETE FROM "+tableName+" WHERE ROWID="+QString::number(rowCount);

        ret = qry.exec(query);

        if(!ret)
        {
            _qDebugE("Query execution failed for deleting Last RowNumber.","","","");
        }
        else
        {
            qry.finish();
            qry.clear();
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - cleanTable
 Parameter(s)  - QString
 Return Type   - bool
 Action        - cleans/deletes all records in the table given in tableName.
 *************************************************************************/
bool Database::cleanTable(QString tableName)
{
    _qDebug("inside bool Database::clearTable(QString tableName).");

    bool ret = true;

    QString query;
    QString err;
    QSqlQuery qry(db);

    performVacuum();

    if(tableName.size() == 0)
    {
        _qDebugE("Oops!! Table name can't be Empty.","","","");

        ret = false;
    }
    else
    {
        query = "DELETE FROM " + tableName;

        ret = qry.exec(query);
        //EX-query.prepare("DELETE FROM RTUInfo");

        if(!ret)
        {
           _qDebugE("Oops!! Query execution failed for clearing table.","","","");
           _qDebugE(qry.lastError().text(),"","","");

        }
        else
        {
           _qDebugE("QUERY IS:[",qry.lastQuery(),"]","");
        }
        qry.finish();
        qry.clear();
    }//else block executes when query prepared successfully

    return ret;
}

/*************************************************************************
 Function Name - getTableNames
 Parameter(s)  - void
 Return Type   - QStringList
 Action        - returns table names of the db.
 *************************************************************************/
QStringList Database::getTableNames()
{
    _qDebug("inside QStringList Database::getTableNames().");

    QStringList list = db.tables();

    _qDebugE("Table Names : ",list,"","");

    QStringList::Iterator it = list.begin();
    while( it != list.end() ) {
      _qDebugE("Table: ",*it,"","");
      ++it;
    }

    return list;
}

/*************************************************************************
 Function Name - getTableData
 Parameter(s)  - QString , QVector<QStringList>&
 Return Type   - bool
 Action        - retrives all data from given table and stores in tableData Buffer.
 *************************************************************************/
bool Database::getTableData(QString tableName,QVector<QStringList>& tableData)
{
    _qDebug("inside bool Database::fetchTable(QString tableName,QVector<QStringList>& tableData).");

    bool ret = true;

    QSqlRecord record;
    QStringList rowData;
    QSqlQuery query(db);

    tableData.clear();

    performVacuum();

    if(tableName.size() == 0)
    {
        _qDebugE("Oops!! Table name can't be Empty.","","","");

        ret = false;
    }
    else
    {
        ret = query.prepare("SELECT * FROM "+ tableName);
        if(!ret)
        {
           _qDebugE("Oops!! query preparation failed.","","","");
        }
        else
        {
            ret = query.exec();
            if(!ret)
            {
               _qDebugE("Oops!! query execution failed.","","","");
               _qDebugE(query.lastError().text(),"","","");
            }
            else
            {
                record = query.record();

                while (query.next())
                {
                    rowData.clear();
                    for(int i=0; i < record.count(); i++)
                    {
                       rowData.append(query.value(i).toString());

                        _qDebugE(query.value(i).toString(),"","","");

                    }
                    tableData.append(rowData);

                    _qDebugE("RowData :[",rowData,"]","");
                }
            }//else block executes when query executes successfully

            query.finish();
            query.clear();

            if(tableData.size() == 0)
            {
                _qDebugE("Oops! data could n't be fetched.","","","");

                ret = false;
            }
        }//else block executes when query prepared successfully
    }

    return ret;
}

/*************************************************************************
 Function Name - getLastTableRowData
 Parameter(s)  - QString, QStringList
 Return Type   - bool
 Action        - retrieves the last row from table given in tableName and
                 stores in rowData Buffer.
 *************************************************************************/
bool Database::getLastTableRowData(QString tableName, QStringList &rowData)
{
    _qDebug("inside bool Database::getLastTableRowData(QString tableName, QStringList &rowData).");

    bool ret = true;
    QSqlQuery query(db);
    QSqlRecord record;

    rowData.clear();

    performVacuum();

    if(tableName.size() == 0)
    {
        _qDebugE("Oops!! Table name can't be Empty.","","","");

        ret = false;
    }
    else
    {
        ret = query.prepare("SELECT * FROM "+ tableName + " WHERE ROWID=(SELECT MAX(ROWID) FROM "+tableName +")") ;

        if(!ret)
        {
           _qDebugE("Oops!! query preparation failed.","","","");
        }
        else
        {
            record = query.record();

            while (query.next())
            {
                for(int i=0; i < record.count(); i++)
                {
                   rowData.append(query.value(i).toString());

                   _qDebugE(query.value(i).toString(),"","","");
                }
            }

            if(rowData.size() == 0)
            {
                _qDebugE("Oops! Fetching data failed.","","","");

                ret = false;
            }
            query.finish();
            query.clear();
        }
    }
    return ret;
}

/*************************************************************************
 Function Name - getRowID
 Parameter(s)  - QString, unsigned, QString, QString
 Return Type   - bool
 Action        - returns RowID/RowNumber that contains cellValue in the given
                 column. If any error then this function returns 0.
 *************************************************************************/
unsigned Database::getRowID(QString tableName,QString columnName,QString cellValue)
{
    _qDebug("Inside unsigned Database::getRowID(QString tableName, QString columnName,QString cellValue).");

    unsigned rowID = 0;

    QString query;
    QString tempStr;
    QSqlQuery qry(db);

    performVacuum();

    if(qry.exec(QCoreApplication::tr("SELECT %1 FROM %2").arg(columnName).arg(tableName)))
    {
        while(qry.next())
        {
            tempStr = qry.value(0).toString().trimmed();
            if(tempStr.compare(cellValue.trimmed()) == 0)
            {
                rowID++;
                break;
            }
            else {
                rowID++;
            }
        }

        qry.finish();
        qry.clear();
    }

    return rowID;
}

/*************************************************************************
 Function Name - getRowCount
 Parameter(s)  - QString
 Return Type   - unsigned
 Action        - returns number of rows present in the given table.
 *************************************************************************/
unsigned Database::getRowCount(QString tableName)
{
    _qDebug("Inside unsigned Database::getRowCount(QString tableName).");

    unsigned rowCount = 0;

    QSqlQuery qry(db);
    QString query;

    performVacuum();

    query = QCoreApplication::tr("SELECT COUNT(*) FROM %1").arg(tableName);
    if(qry.exec(query))
    {
        if(qry.next())
        {
            rowCount = static_cast<unsigned>(qry.value(0).toInt());

            _qDebugE("rowCount:",rowCount,"","");

            qry.finish();
            qry.clear();
        }
    }
    return rowCount;
}

/*************************************************************************
 Function Name - getRowContents
 Parameter(s)  - QString, int, QStringList&
 Return Type   - bool
 Action        - retrieves the row values on the basis of the given ROWID
                 and stores in buffer rowContents.
 *************************************************************************/
bool Database::getRowContents(QString tableName, int ROWID,QStringList &rowContents)
{
    _qDebug("Inside bool Database::getRowContents(QString tableName, int ROWID,QStringList &rowContents).");
    bool ret = true;

    QString query;
    QSqlRecord record;
    QSqlQuery qry(db);

    performVacuum();

    rowContents.clear();

    if((tableName.size() == 0) || (ROWID == 0))
    {
        _qDebugE("Oops!! TableName or ROWID can't be Empty.","","","");

        ret = false;
    }
    else
    {
        query = "SELECT * FROM "+tableName+" WHERE ROWID="+QString::number(ROWID);

        ret = qry.exec(query);

        if(ret)
        {
            while(qry.next())
            {
                record = qry.record();

                for(int i=0; i<record.count(); i++)
                    rowContents.append(qry.value(i).toString());
            }

            qry.finish();
            qry.clear();
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - getRowContents
 Parameter(s)  - QString, QString, QString, QStringList&
 Return Type   - bool
 Action        - retrieves the row values on the basis of the given columnName
                 and currentCellValue and stores in buffer rowContents.
 *************************************************************************/
bool Database::getRowContents(QString tableName, QString columnName,
                                      QString currentCellValue, QStringList& rowContents)
{
    _qDebug("Inside getRowContents(QString tableName, QString columnName,\
            QString currentCellValue, QStringList &rowContents).");
    bool ret = true;

    QString query;
    QSqlQuery qry(db);

    performVacuum();

    rowContents.clear();

    if((tableName.size() == 0) || (columnName.size() == 0) || (currentCellValue.size() == 0))
    {
        _qDebugE("Oops!! TableName or ColumnName or currentCellValue can't be Empty.","","","");

        ret = false;
    }
    else
    {
        query = QCoreApplication::tr("SELECT * FROM %1 WHERE \"%2\"=\"%3\"").arg(tableName).arg(columnName).arg(currentCellValue);

        ret = qry.exec(query);

        if(ret)
        {
            while(qry.next())
            {
                for(int i = 0; i<qry.record().count(); i++)
                {
                    rowContents.append(qry.value(i).toString().trimmed());
                }
            }

            qry.finish();
            qry.clear();
        }
    }

    return ret;
}
/*************************************************************************
 Function Name - getColumnNames
 Parameter(s)  - QString , QStringList&
 Return Type   - bool
 Action        - retrieves the column names of table given in tableName and
                 stores in columnNames.
 *************************************************************************/
bool Database::getColumnNames(QString tableName,QStringList& columnNames)
{
    _qDebug("inside bool Database::getColumnNames(QString tableName,QStringList& columnNames).");

    columnNames.clear();
    bool ret = true;

    QSqlRecord record;
    QStringList rowData;
    QSqlQuery query(db);

    performVacuum();

    if(tableName.size() == 0)
    {
        _qDebugE("Oops!! Table name can't be Empty.","","","");

        ret = false;
    }
    else
    {
        ret = query.prepare("SELECT * FROM "+ tableName);
        if(!ret)
        {
           _qDebugE("Oops!! query preparation failed.","","","");
        }
        else
        {
            ret = query.exec();
            if(!ret)
            {
               _qDebugE("Oops!! query execution failed.","","","");
               _qDebugE(query.lastError().text(),"","","");
            }
            else
            {
                record = query.record();

                if(record.count()<1) ret = false;

                for (int i=0; i<record.count(); i++)
                {
                    columnNames.push_back(record.fieldName(i));
                }
            }//else block executes when query executes successfully

            query.finish();
            query.clear();
        }//else block executes when query prepared successfully

        if(columnNames.count() < 1)
        {
            _qDebugE("Oops! data could n't be fetched.","","","");

            ret = false;
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - getColumnNames
 Parameter(s)  - QSqlTableModel* , QStringList&
 Return Type   - bool
 Action        - retrieves the column names of table given in tableName and
                 stores in columnNames.
 *************************************************************************/
bool Database::getColumnNames(QSqlTableModel* model, QStringList &columnNames)
{
    _qDebug("Inside bool Database::getColumnNames(QSqlTableModel* model, QStringList &columnNames).");
    bool ret = true;

    QSqlRecord record;

    columnNames.clear();

    record = model->record();

    for(int i=0; i<record.count(); i++)
    {
        columnNames.append(record.fieldName(i));
    }

    if(columnNames.count() == 0)
    {
        ret = false;
    }
    return ret;
}

/*************************************************************************
 Function Name - getColumnCount
 Parameter(s)  - QString
 Return Type   - unsigned
 Action        - returns number of columns present in the given table.
 *************************************************************************/
unsigned Database::getColumnCount(QString tableName)
{
    _qDebug("Inside unsigned Database::getColumnCount(QString tableName).");
    unsigned columnCount = 0;

    QStringList columnNames;
    columnNames.clear();

    if(getColumnNames(tableName, columnNames))
    {
        columnCount = static_cast<unsigned>(columnNames.count());
    }

    _qDebugE("columnCount:",columnCount,"","");

    return columnCount;
}

/*************************************************************************
 Function Name - getColumnContents
 Parameter(s)  - QString, QString, QStringList&
 Return Type   - bool
 Action        - retrieves all data from column, whose name is given in
                 "columnName" and stores in buffer columnContents.
 *************************************************************************/
bool Database::getColumnContents(QString tableName, QString columnName, QStringList& columnContents)
{
    _qDebug("Inside bool Database::getColumnContents(QString tableName, QString columnName, QStringList &columnContents).");

    columnContents.clear();
    bool ret = true;

    QSqlQuery qry(db);
    QString query;

    performVacuum();

    if((tableName.size() == 0) || (columnName.size() == 0))
    {
        _qDebugE("Oops!! Table name or Column Name can't be Empty.","","","");

        ret = false;
    }
    else
    {
        query = QCoreApplication::tr("SELECT %1 FROM %2").arg(columnName).arg(tableName);

        ret = qry.exec(query);

        if(!ret)
        {
            _qDebugE("Error! Query Execution Failed.","","","");
        }
        else
        {
            while(qry.next())
            {
                columnContents.append(qry.value(0).toString());
            }

            qry.finish();
            qry.clear();
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - getColumnContents
 Parameter(s)  - QSqlDatabase, QString, QString, QStringList&
 Return Type   - bool
 Action        - retrieves all data from column, whose name is given in
                 "columnName" and stores in buffer columnContents.
 *************************************************************************/
bool Database::getColumnContents(QSqlDatabase DB,QString tableName, QString columnName, QStringList &columnContents)
{
    _qDebug("Inside bool Database::getColumnContents(QSqlDatabase DB,QString tableName, QString columnName, QStringList &columnContents).");

    columnContents.clear();
    bool ret = true;

    QSqlQuery qry(DB);
    QString query;

    performVacuum();

    if((tableName.size() == 0) || (columnName.size() == 0))
    {
        _qDebugE("Oops!! Table name or Column Name can't be Empty.","","","");

        ret = false;
    }
    else
    {
        query = QCoreApplication::tr("SELECT %1 FROM %2").arg(columnName).arg(tableName);

        ret = qry.exec(query);

        if(!ret)
        {
            _qDebugE("Error! Query Execution Failed.","","","");
        }
        else
        {
            while(qry.next())
            {
                columnContents.append(qry.value(0).toString());
            }

            qry.finish();
            qry.clear();
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - getCellContent
 Parameter(s)  - QString, QString, unsigned, QString
 Return Type   - bool
 Action        - retrieves the cell values on the basis of the given columnName
                 and ROWID and stores in buffer cellContent.
 *************************************************************************/
bool Database::getCellContent(QString tableName, QString columnName,
                                          unsigned ROWID, QString &cellContent)
{
    _qDebug("Inside bool Database::getCellContent(QString tableName, QString columnName,\
            unsigned ROWID, QString &cellContent).");

    bool ret = true;

    QString query;
    QSqlQuery qry(db);

    performVacuum();

    cellContent.clear();

    if((tableName.size() == 0) || (columnName.size() == 0) || (ROWID == 0))
    {
        _qDebugE("Oops!! TableName or ColumnName or ROWID can't be Empty.","","","");

        ret = false;
    }
    else
    {
        query = QCoreApplication::tr("SELECT %1 FROM %2 WHERE "
                           "ROWID=%3").arg(columnName).arg(tableName).arg(QString::number(ROWID));

        ret = qry.exec(query);

        if(ret)
        {
            while(qry.next())
            {
                cellContent = qry.value(0).toString();
            }

            qry.finish();
            qry.clear();
        }
    }

    return ret;
}
/*************************************************************************
 Function Name - getDB
 Parameter(s)  - void
 Return Type   - QSqlDatabase
 Action        - returns database object.
 *************************************************************************/
QSqlDatabase Database::getDB()
{
    _qDebug("inside QSqlDatabase Database::getDB().");

    return db;
}

/*************************************************************************
 Function Name - getDBName
 Parameter(s)  - void
 Return Type   - QString
 Action        - returns database name.
 *************************************************************************/
QString Database::getDBName()
{
    _qDebug("inside QString Database::getDBName().");

    return db.databaseName();
}

/*************************************************************************
 Function Name - getDatabaseModel
 Parameter(s)  - QString
 Return Type   - QSqlTableModel*
 Action        - returns pointer to QSqlTableModel type for given TableName.
 *************************************************************************/
QSqlTableModel* Database::getDatabaseModel(QString tableName)
{
    _qDebug("Inside QSqlTableModel* Database::getDatabaseModel(QString tableName).");

    if(model == nullptr)
    {
        model = new QSqlTableModel(nullptr);
    }

    model->setTable(tableName);
    model->select();

    while(model->canFetchMore())
    {
        model->fetchMore();
    }

    return model;
}

/*************************************************************************
 Function Name - getDatabaseModel
 Parameter(s)  - QString, QSqlDatabase
 Return Type   - QSqlTableModel*
 Action        - returns pointer to QSqlTableModel type for given TableName
                 and QSqlDatabase object.
 *************************************************************************/
QSqlTableModel* Database::getDatabaseModel(QString tableName, QSqlDatabase db)
{
    _qDebug("Inside QSqlTableModel* Database::getDatabaseModel(QString tableName, QSqlDatabase db).");

    if(model == nullptr)
    {
        model = new QSqlTableModel(nullptr, db);
    }

    model->setTable(tableName);
    model->select();

    while(model->canFetchMore())
    {
        model->fetchMore();
    }

    return model;
}

/*************************************************************************
 Function Name - setFilter
 Parameter(s)  - QSqlTableModel*, QString
 Return Type   - void
 Action        - sets given filter to given model.
 *************************************************************************/
void Database::setFilter(QSqlTableModel* model, QString filter)
{
    _qDebug("Inside void Database::setFilter(QSqlTableModel *model, QString filter).");

    if(!filter.isEmpty())
        model->setFilter(filter);

    while(model->canFetchMore())
    {
        model->fetchMore();
    }
}

/*************************************************************************
 Function Name - lastError
 Parameter(s)  - void
 Return Type   - QString
 Action        - returns last error string of database if present any.
 *************************************************************************/
QString Database::lastError()
{
    _qDebug("Inside QString Database::lastError().");

    return db.lastError().text();
}

/*************************************************************************
 Function Name - performVacuum
 Parameter(s)  - void
 Return Type   - bool
 Action        - cleans the main database by copying its contents to a
                 temporary database file and reloading the original database
                 file from the copy. This eliminates free pages, aligns table
                 data to be contiguous, and otherwise cleans up the database
                 file structure.
                 VACUUM command may change the ROWID of entries in tables that
                 do not have an explicit INTEGER PRIMARY KEY. The VACUUM command
                 only works on the main database. It is not possible to VACUUM an
                 attached database file.
 *************************************************************************/
bool Database::performVacuum()
{
    _qDebug("Inside bool Database::performVacuum().");

    QSqlQuery qry(db);
    bool ok = qry.exec("VACUUM");
    qry.finish();
    qry.clear();
    return ok;
}

/*************************************************************************
 Function Name - isDataPresentInColumn
 Parameter(s)  - QString, QString, QString
 Return Type   - bool
 Action        - returns true if the CellValue is found under ColumnName in
                 the given TableName or else returns false.
 *************************************************************************/
bool Database::isDataPresentInColumn(QString tableName, QString columnName, QString data)
{
    _qDebug("Inside bool Database::isDataPresentInColumn(QString tableName, QString columnName, QString data).");

    bool ret = false;

    QSqlQuery qry(db);
    QString query;
    QString temp;

    performVacuum();

    if((tableName.size() == 0) || (columnName.size() == 0))
    {
        _qDebugE("Oops!! Table name and ColumnName can't be Empty.","","","");
    }
    else
    {
        query = QCoreApplication::tr("SELECT %1 FROM %2").arg(columnName).arg(tableName);

        ret = qry.exec(query);

        if(ret)
        {
            while(qry.next())
            {
                temp = qry.value(0).toString();

                if(temp.compare(data) == 0)
                {
                    ret = true;
                    break;
                }
            }

            qry.finish();
            qry.clear();
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - renameTable
 Parameter(s)  - QString, QString
 Return Type   - bool
 Action        - renames the existing table.
 *************************************************************************/
bool Database::renameTable(QString oldTableName, QString newTableName)
{
    _qDebug("Inside bool Database::renameTable(QString TableName, QString NewName).");

    bool ret = false;

    QStringList allTables;
    QString query;
    QSqlQuery qry(db);

    performVacuum();

    if((oldTableName.size() == 0) || (newTableName.size() == 0))
    {
        _qDebugE("Oops!! OldTableName or NewTableName can't be Empty","","","");
    }
    else
    {
        allTables = getTableNames();

        //Before Renaming check whether there exists another table with same name
        if(allTables.contains(newTableName))
        {
            _qDebugE("Error! Given NewTableName is already Exist.","","","");
        }
        else
        {
            if(!isTableNameValid(newTableName))//check for valid NewTableName entry
            {
                _qDebugE("Oops! Invalid NewTableName Entry.","","","");
            }
            else
            {
                query = QCoreApplication::tr("ALTER TABLE %1 RENAME TO %2").arg(oldTableName).arg(newTableName);

                ret = qry.exec(query);

                qry.finish();
                qry.clear();
            }
        }
    }
    return  ret;
}

/*************************************************************************
 Function Name - isTableNameValid
 Parameter(s)  - QString
 Return Type   - bool
 Action        - checks and returns true if valid TableName or else returns
                 false.
 *************************************************************************/
bool Database::isTableNameValid(QString tableName)
{
    _qDebug("Inside bool Database::isTableNameValid(QString tableName).");
    bool ret = true;

    if(tableName.isEmpty())
    {
        _qDebugE("The TableName can not be empty.","","","");
        ret = false;
    }
    else if(tableName.contains(" "))
    {
        _qDebugE("The TableName can not contain whitespace character.","","","");
        ret = false;
    }
    else if(tableName.contains("-"))
    {
        _qDebugE("The TableName can not contain '-' character.","","","");
        ret = false;
    }
    else if(tableName.at(0)>= '0' && tableName.at(0)<= '9')
    {
        _qDebugE("The TableName can not begin with numeric character.","","","");
        ret = false;
    }
    return ret;
}

/*************************************************************************
 Function Name - generateQueryForCreation
 Parameter(s)  - QString , QString& , QStringList
 Return Type   - void
 Action        - generates a query for table creation with name given in
                 tableName and ceates columns in the table with column names
                 given in tableColumnNames.
 *************************************************************************/
void Database::generateQueryForCreation(QString tableName,QString& queryGenerated,QStringList tableColumnNames)
{
    _qDebug("inside void Database::generateQueryForCreation(QString tableName,QString& queryGenerated,QStringList tableColumnNames).");

    queryGenerated.clear();

    QString fields;
    fields.clear();

    for (int i = 0; i < tableColumnNames.size(); i++) {
          _qDebugE("columnNames : ",i," : ",tableColumnNames.at(i));

          if(i == 0) fields.append("(\"");
          else fields.append("\"");
          fields.append(tableColumnNames.at(i) + "\" varchar(500)");
          if(i == tableColumnNames.size() - 1) fields.append(")");
          else fields.append(", ");
    }

    //query.exec("create table if not exists trTable(\"To\" varchar(500), \"From\" varchar(500))");
    queryGenerated = "create table if not exists " + tableName + fields;

    _qDebugE("queryGenerated : ",queryGenerated,"","");
}

/*************************************************************************
 Function Name - generateQueryForInsertion
 Parameter(s)  - QString , QString& , QStringList&
 Return Type   - void
 Action        - generates a query for a record insertion in given table in
                 tableName.
 *************************************************************************/
void Database::generateQueryForInsertion(QString tableName,QString& queryGenerated,QStringList& bindValues)
{
    _qDebug("inside void Database::generateQueryForInsertion(QString tableName,QString& queryGenerated,QStringList& bindValues).");

    queryGenerated.clear();
    bindValues.clear();

    QString fields1 = "(";
    QString fields2 = "(";

    QSqlRecord record = db.record(tableName);

    _qDebugE("field count : ",record.count(),"","");

    for (int i = 0; i < record.count(); i++) {
          _qDebugE("Field ",i," named ",record.fieldName(i));

          bindValues.append(":"+record.fieldName(i));
          fields1.append(record.fieldName(i));
          fields2.append(":"+record.fieldName(i));
          if(i != record.count() - 1)
          {
              fields1.append(", ");
              fields2.append(", ");
          }
          else
          {
              fields1.append(")");
              fields2.append(")");
          }
    }

    queryGenerated = "INSERT INTO " + tableName + " "+fields1 + " VALUES " + fields2;

    _qDebugE("queryGenerated : ",queryGenerated,"","");
    _qDebugE("Bind values : ",bindValues,"","");
}

/*************************************************************************
 Function Name - generateQueryForDeletion
 Parameter(s)  - QString , QString& , QStringList&, QStringList
 Return Type   - void
 Action        - generates a query for table record deletion, the record
                 having data given in parameter data.
 *************************************************************************/
void Database::generateQueryForDeletion(QString tableName,QString& queryGenerated,QStringList& bindValues,QStringList data)
{
    _qDebug("inside void Database::generateQueryForDeletion(QString tableName,QString& queryGenerated,QStringList& bindValues,QStringList data).");

    queryGenerated.clear();
    bindValues.clear();

    QString fields;

    QSqlRecord record = db.record(tableName);

    _qDebugE("field count : ",record.count(),"","");

    for (int i = 0; i < record.count(); i++)
    {
      _qDebugE("Field ",i," named ",record.fieldName(i));

      if(data.at(i) != "")
      {
          if(!fields.isEmpty())
          {
              fields.append(" AND ");
          }
          fields.append(record.fieldName(i));
          fields.append("=");
          fields.append("(:"+record.fieldName(i)+")");
      }
      bindValues.append(":"+record.fieldName(i));
    }

    //query.prepare("DELETE FROM Accounts WHERE ID=(:ID) AND PW=(:PW) AND Type=(:Type)");
    queryGenerated = "DELETE FROM " + tableName + " WHERE "+fields;

    _qDebugE("queryGenerated : ",queryGenerated,"","");
    _qDebugE("fields : ",fields,"","");
    _qDebugE("Bind values : ",bindValues,"","");
}

/*************************************************************************
 Function Name - generateQueryForSelection
 Parameter(s)  - QString, QString&, QStringList&, QString, QStringList
 Return Type   - void
 Action        - generates a query for selecting a record whose data matches
                 with the matchData.
 *************************************************************************/
void Database::generateQueryForSelection(QString tableName,QString& queryGenerated,QStringList& bindValues,QString matchData,QStringList data)
{
    _qDebug("inside void Database::generateQueryForSelection(QString tableName,QString& queryGenerated,QStringList& bindValues,QString matchData,QStringList data).");

    queryGenerated.clear();
    bindValues.clear();

    QString fields;

    QSqlRecord record = db.record(tableName);

    _qDebugE("field count : ",record.count(),"","");

    for (int i = 0; i < record.count(); i++) {
      _qDebugE("Field ",i," named ",record.fieldName(i));

      if(data.at(i) != "")
      {
          if(!fields.isEmpty())
          {
              fields.append(" AND ");
          }
          fields.append(record.fieldName(i));
          fields.append("=");
          fields.append("(:"+record.fieldName(i)+")");
      }
      bindValues.append(":"+record.fieldName(i));
    }

    //query.prepare("SELECT Type FROM Accounts WHERE ID=(:ID) AND PW=(:PW)");
    queryGenerated = "SELECT "+ matchData +" FROM "+ tableName + " WHERE "+fields;
}

/*************************************************************************
 Function Name - generateQueryForUpdation
 Parameter(s)  - QString, QString&, QStringList&, QStringList, QStringList
 Return Type   - void
 Action        - generates a query for updating an existing record. It
                 searches the values present in dataToMatch and replaces
                 with dataToUpdate.
 *************************************************************************/
void Database::generateQueryForUpdation(QString tableName,QString& queryGenerated,QStringList& bindValues,QStringList dataToUpdate,QStringList dataToMatch)
{
    _qDebug("inside void Database::generateQueryForUpdation(QString tableName,QString& queryGenerated,QStringList& bindValues,QStringList dataToUpdate,QStringList dataToMatch).");

    queryGenerated.clear();
    bindValues.clear();

    QString fields1;
    QString fields2;

    QSqlRecord record = db.record(tableName);

    fields1.clear();
    fields2.clear();

    _qDebugE("record count : ",record.count(),"","");

    for (int i = 0; i < record.count(); i++)
    {
          _qDebugE("Field ",i," named ",record.fieldName(i));

          bindValues.append(":"+record.fieldName(i));

          if(dataToUpdate.at(i) != "")
          {
              if(!fields1.isEmpty())
              {
                  fields1.append(" , ");
              }

              fields1.append(record.fieldName(i) + " = :" + record.fieldName(i));
          }

          if(dataToMatch.at(i) != "")
          {
              if(!fields2.isEmpty())
              {
                  fields2.append(" , ");
              }

              fields2.append(record.fieldName(i) + " = :" + record.fieldName(i));
          }
    }

    //query.prepare(UPDATE Accounts SET PW = '111' AND Type = 'admin' WHERE ID = 'HAL')
    queryGenerated = "UPDATE " + tableName + " SET "+ fields1 + " WHERE " + fields2;
}

/*************************************************************************
 Function Name - generateQueryForAllRowsDeletion
 Parameter(s)  - QString, QString&
 Return Type   - void
 Action        - generates a query for deleting all records from given
                 table tableName
 *************************************************************************/
void Database::generateQueryForclearTable(QString tableName, QString& queryGenerated)
{
    _qDebug("inside void Database::generateQueryForclearTable(QString tableName, QString& queryGenerated).");

    queryGenerated.clear();

    //query.prepare(UPDATE Accounts SET PW = '111' AND Type = 'admin' WHERE ID = 'HAL')
    queryGenerated = "DELETE FROM " + tableName;
}
