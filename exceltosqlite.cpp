#include "exceltosqlite.h"
#include <QStringList>
#include <QString>

excelToSQLite::excelToSQLite()
{
    database = new Database();
}

excelToSQLite::~excelToSQLite()
{
    qDebug("Inside excelToSQLite::~excelToSQLite()");
    database->closeDB();
    delete database;
}


int excelToSQLite::createTable(QStringList strList)
{
    qDebug()<<strList;
    return 0;
}

void excelToSQLite::openDb(QString dataBasePath)
{
    qDebug("Inside void excelToSQLite::openDb(QString dataBasePath)");
    qDebug()<<"DataBase Path = "<<dataBasePath;
    QStringList temp = dataBasePath.split("/");
    qDebug()<<temp;
    QString dataBaseName = temp.at(temp.count() - 1);
    qDebug()<<"dataBaseName = "<<dataBaseName;
    if(database->openDB(dataBasePath,"localHost","Tarun","tarun","DataBase"))
    {
        qDebug("DataBase opened successfully");
    }
    qDebug("Exiting void excelToSQLite::openDb(QString dataBasePath)");
}

void excelToSQLite::openExcel(QString excelFilePath)
{
    qDebug("Inside void excelToSQLite::openExcel(QString excelFilePath)");
    QXlsx::Document excelFile(excelFilePath);
    qDebug()<<"dimensions = "<<excelFile.dimension().rowCount()<<" x "<<excelFile.dimension().columnCount();
    int columnCounts = 0;
    QStringList columnHeaders;
    while(1)
    {
        QString temp = excelFile.read(1,columnCounts + 1).toString().trimmed();
        if(temp == "")
            break;
        columnHeaders.append(temp);
        columnCounts++;
    }
    qDebug()<<"Table Headers = "<<columnHeaders<<" column Counts = "<<columnCounts;

    QStringList temp = excelFilePath.split("/");
    qDebug()<<temp;
    QString tableName = temp.at(temp.count() - 1).split(".").at(0);
    qDebug()<<"tableName = "<<tableName;
    database->createTable(tableName,columnHeaders);
    QVector<QStringList> rowsData;

    for(int i = 2; i <= excelFile.dimension().rowCount(); i++)
    {
        int tempCounter = 0;
        QStringList tempRowData;
        for(int j = 1; j <= columnCounts; j++) {
            QString tempCellData = excelFile.read(i,j).toString().trimmed();
            if(tempCellData == "")
                tempCounter++;
            tempRowData.append(tempCellData);
        }
        qDebug()<<"rowNo = "<<i<<" Data = "<<tempRowData;
        if(tempCounter == columnCounts)
            break;
        rowsData.append(tempRowData);
    }
    database->addMultipleRowsData(tableName,rowsData);
//    database->closeDB();
    qDebug("Exiting void excelToSQLite::openExcel(QString excelFilePath)");
}

void excelToSQLite::createExcel(QString tableName,QString ExcelFilepath)
{
    qDebug("Inside void excelToSQLite::createExcel(QString tableName,QString ExcelFilepath)");
    QXlsx::Document ExcelFile(ExcelFilepath);
    int rowCount = database->getRowCount(tableName);
    int ColumnCount = database->getColumnCount(tableName);
    qDebug()<<"Table Dimensions: "<<rowCount<<" x "<<ColumnCount;
    if(!database->isOpen())  {
        qDebug("DataBase is closed");
        return;
    }
    QSqlTableModel *tablemodel = database->getDatabaseModel(tableName);
    QStringList headers;
    database->getColumnNames(tableName,headers);
    qDebug()<<"Table Headers:"<<headers;
    for(int i = 0; i < ColumnCount; i++)
    {
//        QVariant value = QVariant(headers.at(i));
        qDebug()<<"Adding Column Headers"<<headers.at(i);
        ExcelFile.write(1,i+1,headers.at(i));
    }
    QVector<QStringList> tableData;
    database->getTableData(tableName,tableData);
    for(int i = 0; i < rowCount; i++)
    {
        QStringList rowData = tableData.at(i);
        qDebug()<<"Row Number: "<<i<<" Data: "<<rowData;
        for(int j = 0; j < ColumnCount; j++)
        {
            ExcelFile.write(i+2, j+1, rowData.at(j));
//            qDebug()<<"Data["<<i<<"]["<<j<<"] = "<<rowData.at(j);
//            QVariant value = QVariant(rowData.at(j));
//            ExcelFile.write(i+2, j+1, value);
        }
    }
    if(ExcelFile.save())
    {
        qDebug("Excel File saved Successfully.");
    }
    qDebug("Exiting void excelToSQLite::createExcel(QString tableName,QString ExcelFilepath)");
}


QStringList excelToSQLite::getTablesList()
{
    qDebug("Inside QStringList excelToSQLite::getTablesList()");
    qDebug("Inside QStringList excelToSQLite::getTablesList()");
    return database->getTableNames();
}

QSqlTableModel* excelToSQLite::getSqlTable(QString TableName)
{
    return database->getDatabaseModel(TableName);
}

