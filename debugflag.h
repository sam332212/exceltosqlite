#ifndef DEBUGFLAG_H
#define DEBUGFLAG_H
#define clearmem(x) delete(x);x=nullptr
#define PCI_1758    1

//    Signal_Name	DOP_Number
#define RLC_P2A		501
#define RLC_Q2A		502
#define RLC_R2B		503
#define RLC_S2B		504
#define RLC_DMM		505
#define RLC_MEG		506
#define RLC_ERH		507
#define RLC_LT1		508
#define RLC_LT2		509
#define RLC_LT3		510
#define RLC_RES		511

#undef GLOBAL_DEBUG
#define GLOBAL_DEBUG  1// Change 1 to 0 to remove all General Debug msg's.
#if GLOBAL_DEBUG
    #define MAIN_DEBUG              1

    #define DATABASE_DEBUG          0
    #define DATABASE_DEBUG_E        0

    #define DMM_DEBUG               1
    #define DMM_DEBUG_E             1

    #define IRMETER_DEBUG           1
    #define IRMETER_DEBUG_E         1

    #define PS_DEBUG                1
    #define PS_DEBUG_E              1

    #define EDITUSER_DEBUG          1
    #define EDITUSER_DEBUG_E        1

    #define HOME_DEBUG              1
    #define HOME_DEBUG_E            1

    #define LOGIN_DEBUG             1
    #define LOGIN_DEBUG_E           1

    #define SERIALPORT_DEBUG        1
    #define SERIALPORT_DEBUG_E      1

    #define SIGNUP_DEBUG            1
    #define SIGNUP_DEBUG_E          1

    #define UTILITY_DEBUG           1
    #define UTILITY_DEBUG_E         1

    #define TIMER_DEBUG             0

    #define ISOLATION_TEST_DEBUG    1
    #define ISOLATION_TEST_DEBUG_E  1

    #define INSULATION_TEST_DEBUG   1
    #define INSULATION_TEST_DEBUG_E 1

    #define FUNCTIONAL_TEST_DEBUG   1
    #define FUNCTIONAL_TEST_DEBUG_E 1

    #define LOAD_TEST_DEBUG         1
    #define LOAD_TEST_DEBUG_E       1

    #define DEVICESHANDLER_DEBUG    1
    #define DEVICESHANDLER_DEBUG_E  1

    #define PCI1758_DEBUG           1
    #define PCI1758_DEBUG_E         1

    #define PCI7444_DEBUG           1
    #define PCI7444_DEBUG_E         1

    #define ISOLATIONP2C_DEBUG      1
    #define ISOLATIONP2C_DEBUG_E    1

    #define ISOLATIONP2P_DEBUG      1
    #define ISOLATIONP2P_DEBUG_E    1

    #define INSULATION_DEBUG        1
    #define INSULATION_DEBUG_E      1
#else
    #define MAIN_DEBUG              0

    #define DATABASE_DEBUG          0
    #define DATABASE_DEBUG_E        0

    #define DMM_DEBUG               0
    #define DMM_DEBUG_E             0

    #define IRMETER_DEBUG           0
    #define IRMETER_DEBUG_E         0

    #define PS_DEBUG                0
    #define PS_DEBUG_E              0

    #define EDITUSER_DEBUG          0
    #define EDITUSER_DEBUG_E        0

    #define HOME_DEBUG              0
    #define HOME_DEBUG_E            0

    #define LOGIN_DEBUG             0
    #define LOGIN_DEBUG_E           0

    #define SERIALPORT_DEBUG        0
    #define SERIALPORT_DEBUG_E      0

    #define SIGNUP_DEBUG            0
    #define SIGNUP_DEBUG_E          0

    #define TIMER_DEBUG             0

    #define ISOLATION_TEST_DEBUG    0
    #define ISOLATION_TEST_DEBUG_E  0

    #define INSULATION_TEST_DEBUG   0
    #define INSULATION_TEST_DEBUG_E 0

    #define FUNCTIONAL_TEST_DEBUG   0
    #define FUNCTIONAL_TEST_DEBUG_E 0

    #define LOAD_TEST_DEBUG         0
    #define LOAD_TEST_DEBUG_E       0

    #define DEVICESHANDLER_DEBUG    0
    #define DEVICESHANDLER_DEBUG_E  0

    #define ISOLATIONP2C_DEBUG      0
    #define ISOLATIONP2C_DEBUG_E    0

    #define ISOLATIONP2P_DEBUG      0
    #define ISOLATIONP2P_DEBUG_E    0

    #define INSULATION_DEBUG        0
    #define INSULATION_DEBUG_E      0

    #define PCI1758_DEBUG           0
    #define PCI1758_DEBUG_E         0

    #define PCI7444_DEBUG           0
    #define PCI7444_DEBUG_E         0
#endif
#endif // DEBUGFLAG_H
